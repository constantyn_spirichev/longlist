import React, { Component, PropTypes } from 'react';
import marked from 'react-marked';

import styles from './index.css';

export default class App extends Component {
    //static defaultProps = {};

  constructor(props){
    super();
    this.state = {
        collection: props.collection,
        pattern: ''
      };
  };

  render() {
    const collection = this.state.collection;
    const pattern = this.state.pattern;
    var items = this.state.collection.cities.models.map((item)=>{
      var output = [], stations;
          output.push(<CityItem label={item.label} key={'ci'+item.id} pattern={pattern}/>);
          if (item.stations.length > 1) {
            stations = item.stations.map((sationId)=>{
              const station = collection.stations.getById(sationId)
              if (station) {
                return <StationItem label={station.label} key={'st'+station.id} pattern={pattern} />
              }
            });
            output.push(stations);
          }
      return output;
    })
    return (
      <div>
      <Counter count={this.state.collection.length} searchTime ={this.state.searchTime}/>
      <ol>
        {items}
      </ol>
      </div>
    );
  }

  componentDidMount() {
    const input =  document.getElementById("search");
    const collection = this.props.collection;
    var _this = this;
    function handler(e) {
      var searchTime = (new Date()).getTime();
      var searchResults = collection.search(this.value, undefined, true);
      searchTime = (new Date()).getTime() - searchTime;
      _this.setState({collection: searchResults, pattern: this.value, searchTime});
    }
    input.addEventListener("keyup", handler)
  }
};

class CityItem extends Component {
  render() {
    const regExp = new RegExp('('+this.props.pattern+')', 'i');
    const label = this.props.pattern ? this.props.label.replace(regExp, "__$1__") : this.props.label;

    return (
      <li key={this.props.key} className="cityItem">{marked(label)}</li>
    )
  };
};

class StationItem extends Component {
  render() {
    const regExp = new RegExp('('+this.props.pattern+')', 'i');
    const label = this.props.pattern ? this.props.label.replace(regExp, "__$1__") : this.props.label;
    return (
      <li key={this.props.key} className={styles.stationItem}>{marked(label)}</li>
    )
  };
};

class Counter extends Component {
  render() {
    return (
      <div className={styles.counter}>
        total records: {this.props.count}
        <br />
        <small> includes single stations that not shown </small>
        <br />
        <small>was performed in {this.props.searchTime || 0} ms</small>
        <br />
        <small>sorting time {window.sortTime || 0} ms </small>
      </div>
    )
  };
}

class CatygoryItem extends Component {
  render() {
    return (
      <li>"A"</li>
    )
  };
}
