import 'babel-polyfill';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './containers/App/index';
import AutoCompleteCollection from './helpers/StationsCollection'


Promise.all([fetch('/data/cities.json'), fetch('/data/stations.json')])
        .then(function(response){
          return Promise.all([response[0].json(), response[1].json()])
                  .then(function(data){
                    return {
                      cities: data[0],
                      stations: data[1],
                    }
                  })
        }).then(function(data){
          return  new AutoCompleteCollection(data)
        }).then(function(data){
          ReactDOM.render(
            <App collection={data} />,
            document.getElementById('content')
          );

        })
