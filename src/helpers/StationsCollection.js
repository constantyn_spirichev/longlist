import _ from 'underscore'

function translitMatchString(matchString) {
  matchString = matchString.replace(/ä/g, 'a')
    .replace(/Ä/ig, 'a')
    .replace(/Ö/ig, 'o')
    .replace(/Ü/ig, 'u')
    .replace(/ae/ig, 'a')
    .replace(/oe/ig, 'o')
    .replace(/ue/ig, 'u')
    .replace(/ss/g, 's')
    .replace(/ß/g, 's');
  return matchString;
};

function normalizeMatchString(matchString) {
  // Taken from: https://codepoints.net/basic_multilingual_plane
  // ru locale has some wierd sequence of symbols
  var latin1SupplementSequence = "\u00C0-\u00FF",
    latinExtendedA = "\u0100-\u017F",
    latinExtendedB = "\u0180-\u024F",
    cyrillicSequence = "\u0400-\u04FF",
    cyrillicSupplementSequence = "\u0500-\u052F",
    lettersMatchSequence = latin1SupplementSequence + latinExtendedA + latinExtendedB + cyrillicSequence + cyrillicSupplementSequence,
    sanitizerRegex = "/[\w" + lettersMatchSequence + "]/g";

  return matchString.replace(sanitizerRegex, ' ');
};

function getDestinations(cityUrl, stationUrl) {
  var city_pairs = new $.Deferred(),
    station_pairs = new $.Deferred(),
    savedHash = localStorage.getItem("autocomplete_hash");

  cityUrl = cityUrl || '/location/city-pairs';
  stationUrl = stationUrl || '/location/station-pairs';

  if (localStorage.getItem('city_pairs') && localStorage.getItem('station_pairs') && savedHash === AUTOCOMPLETE_HASH) {
    city_pairs.resolve(JSON.parse(localStorage.getItem('city_pairs')));
    station_pairs.resolve(JSON.parse(localStorage.getItem('station_pairs')));
  } else {
    $.get(cityUrl, function(data, status, jqXHR) {
      city_pairs.resolve(data);
      localStorage.setItem('city_pairs', jqXHR.responseText);
    });
    $.get(stationUrl, function(data, status, jqXHR) {
      station_pairs.resolve(data);
      localStorage.setItem('station_pairs', jqXHR.responseText);
    });
    localStorage.setItem('autocomplete_hash', AUTOCOMPLETE_HASH);
  }
  return $.when(city_pairs, station_pairs);
};

export class AutoCompleteModels {
  constructor(data) {
    this.models = [];
    this._byId = data || {};

    if (typeof data !== "undefined") {
      _.each(data, function(item, index) {
        this.models.push(item);
      }, this);

      this.models = _.sortBy(this.models, 'name');
    }

  };

  getById(id) {
    return this._byId[id] || null;
  };

  addModel(data) {
    if (typeof data === "undefinded" || typeof data.id === "undefinded") {
      console.error(new Error("nothing to add or id attribute is missing"));
      return;
    }
    this._byId[data.id] = data;
    this.models.push(data);

    //this.models = _.sortBy(this.models, 'name');
  };

  seekItems(start, number) {
    var result = new AutoCompleteModels();

    start = start || 0;
    number = number ? (number + start) : this.models.length;

    for (var i = start || 0; i < number; i++) {
      if (this.models[i]) {
        result.addModel(this.models[i]);
      } else {
        break;
      }
    }
    return result;
  };
}

export default class AutoCompleteCollection {
  constructor(data) {
    this.cities = data ? new AutoCompleteModels(data.cities) : new AutoCompleteModels();
    this.stations = data ? new AutoCompleteModels(data.stations) : new AutoCompleteModels();

    this.setLength();

    data && this._setMatchString();
  };

  setLength() {
    this.length = this.cities.models.length + this.stations.models.length;
  };

  _setMatchString() {

    _.map(this.stations.models, function(item, index) {
      var translited;

      item.matchString = item.name + " " + item.aliases + " ";
      translited = translitMatchString(item.matchString);
      item.matchString += translited == item.matchString ? "" : translited;
      item.matchString = normalizeMatchString(item.matchString);

    }, this);

    _.map(this.cities.models, function(item, index) {
      var translited;

      item.matchString = item.name + " " + item.aliases;
      translited = translitMatchString(item.matchString);
      item.matchString += translited == item.matchString ? "" : translited;
      item.matchString = normalizeMatchString(item.matchString);

      _.map(this.getStationsInCity(item.id).models, function(station) {
        item.matchString += " " + station.matchString;
      });
    }, this);
  };

  getStationsInCity(id) {
    var city = this.cities._byId[id] || {};
    var stations = {};

    if (city.stations && city.stations.length) {
      _.map(city.stations, function(item) {
        if (this.stations._byId[item]) {
          stations[item] = this.stations._byId[item];
        }
      }, this);
    }
    return new AutoCompleteModels(stations);
  };

  search(pattern, propertie, relevant) {
    var result = new AutoCompleteCollection();
    var regExp = typeof pattern == "object" ? pattern : new RegExp(pattern, "i");
    var matchProp = propertie || "matchString";

window.searchTime = (new Date()).getTime();
    _.map(this.cities.models, function(item) {
      if (regExp.test(item[matchProp])) {
        var copyItem = {...item}
        if (relevant) {
          copyItem.index = item[matchProp].search(regExp) ? 2 : 0;
          if(copyItem.index===2){
            copyItem.index = item[matchProp].search(new RegExp(regExp.source[0], "i")) === 0 ? 1 : 2;
          }
        }
        result.cities.addModel(copyItem);
      }
    }, this);

    _.map(this.stations.models, function(item) {
      if (regExp.test(item[matchProp])) {
        var copyItem = {...item}
        if (relevant) {
            copyItem.index = item[matchProp].search(regExp) ? 2 : 0;
            if(copyItem.index===2){
              copyItem.index = item[matchProp].search(new RegExp(regExp.source[0], "i")) === 0 ? 1 : 2;
            }
        }
        result.stations.addModel(item);
      }
    }, this);

    result.setLength();
    window.sortTime = (new Date()).getTime();
    if(relevant){
      result.cities.models = _.sortBy(result.cities.models, 'index');
      result.stations.models = _.sortBy(result.stations.models, 'index');
    }
    window.sortTime = (new Date()).getTime() - window.sortTime;

    window.searchTime = (new Date()).getTime() - window.searchTime;

    return result;
  };

  injectDestinations(data) {
    if (data && data.cities && data.stations) {
      this.constructor.prototype.destinations = {
        cities: new AutoCompleteModels(data.cities),
        stations: new AutoCompleteModels(data.stations)
      };
    } else {
      console.error("destinations wasn't injected: not valid data");
    }
  };

  filterDestination(cityId, stationId) {
    // @TODO add few combinations of expresions
    // for passed Id's to keep consistance
    if (this.destinations && cityId) {
      var result = new AutoCompleteCollection();
      var toCities = this.destinations.cities.getById(cityId).destinations;
      var toStations = stationId ? this.destinations.stations.getById(stationId).destinations : null;

      _.map(toCities, function(item) {
        var city = this.cities.getById(item);

        city !== null && result.cities.addModel(city);

      }, this);

      if (toStations !== null) {
        _.map(toStations, function(item) {
          var station = this.stations.getById(item);

          station !== null && result.stations.addModel(station);

        }, this);

      } else {
        result.stations = this.stations;
      }

    } else {
      return this;
    }

    result.cities.models = _.sortBy(result.cities.models, 'name');
    result.stations.models = _.sortBy(result.stations.models, 'name');

    result.setLength();

    return result;
  };

  getDestinations() {
    getDestinations.call(this, arguments);
  };
};
